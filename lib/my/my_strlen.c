/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_strlen.c
*/

#include <stdlib.h>

int	my_strlen(char const *str)
{
	int	i = 0;

	if (str == NULL)
		return (0);
	while (str[i] != 0)
		i++;
	return (i);
}
