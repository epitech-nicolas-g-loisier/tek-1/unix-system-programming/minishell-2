/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_compute_power_it.c
*/

long	my_compute_power_it(long nb, long p)
{
	long	pow;

	pow = nb;
	if (p == 0)
		return (1);
	while (p > 1)
	{
		pow = pow * nb;
		p--;
	}
	return (pow);
}
