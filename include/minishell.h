/*
** EPITECH PROJECT, 2018
** Minishell 1
** File description:
** Minishell 1
*/

#ifndef MINISHELL_H_
#define MINISHELL_H_

/* cd_function */
int	print_cd_error(char*);
char	*go_to_home(char**);
int	cd_function(char**, char**);

/* env_function */
int	env_function(char**, char**);
char	**alloc_env(char **);
void	free_env(char**);

/* exit_function.c */
int	exit_function(char**, char***, int);

/* get_all_command.c */
char	***get_all_command(char *);

/* get_command.c */
int	count_arg(char*);
char	*get_arg(char*, int);
char	**command_parser(char*);

/* get_path.c */
char	*get_path_line(char**);
char	*get_path(char*, int);
int	count_path(char*);
char	**path_parser(char*);

/* get_return_value.c */
int	get_return_value(int, int, char*);

/* memory_function */
int	free_command(char***);

/* other_function.c */
int	launch_binary(char**, char**);
int	find_prog(char**, char**);
int	other_function(char**, char**);

/* setenv_function.c */
char	*get_name(char*, char*);
int	check_name(char*, char*);
char	**get_new_env(int, char**, char*);
char	**setenv_function(char**, char**, int*);

/* shell.c */
int	spe_env_function(char**, char***, int);
int	choose_command(char**, char***);
int	launch_minishell(char**);

/* unsetenv_function.c */
int	find_name(char*, char*);
char	**delete_name(int, char**);
char	**unsetenv_function(char**, char**, int*);

#endif /* MINISHELL_H_ */
