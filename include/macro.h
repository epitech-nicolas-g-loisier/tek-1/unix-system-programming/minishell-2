/*
** EPITECH PROJECT, 2018
** Lib
** File description:
** Usefull Macro
*/

#ifndef MACRO_H_
#define MACRO_H_

#define IS_MIN_ALPHA(c)	((c) >= 'a' && (c) <= 'z')
#define IS_MAJ_ALPHA(c)	((c) >= 'A' && (c) <= 'Z')
#define IS_NUM(c)	((c) >= '0' && (c) <= '9')
#define IS_ALPHA(c)	(IS_MIN_ALPHA(c) || IS_MAJ_ALPHA(c))

#endif /* MACRO_H_ */
