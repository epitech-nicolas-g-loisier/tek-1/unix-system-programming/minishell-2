/*
** EPITECH PROJECT, 2018
** Minishell1
** File description:
** get the command enter in the command interpreter
*/

#include <unistd.h>
#include <stdlib.h>
#include "my.h"
#include "minishell.h"

int	count_arg(char *command)
{
	int count = 0;
	int count_arg = 0;

	while (command[count] != '\0') {
		if (command[count] == ' ' || command[count] == '\t')
			count_arg++;
		while (command[count] == ' ' || command[count] == '\t') {
			count++;
		}
		count++;
	}
	count_arg++;
	return (count_arg);
}

char	*get_arg(char *command, int idx)
{
	int count = idx;
	int counter = 0;
	char *arg;

	while ((command[count] != ' ' && command[count] != '\t')
		&& command[count] != '\0') {
		count++;
	}
	arg = malloc(sizeof(char) * (count - idx + 1));
	if (arg == NULL)
		exit(84);
	while (idx < count){
		arg[counter] = command[idx];
		idx++;
		counter++;
	}
	arg[counter] = '\0';
	return (arg);
}

char	**command_parser(char *command)
{
	int count = 0;
	int idx = 0;
	int nb_arg = count_arg(command);
	char **tab = malloc(sizeof(char*) * (nb_arg + 2));

	while (command[idx] != '\0') {
		if (command[idx] != ' ' && command[idx] != '\t'){
			tab[count] = get_arg(command, idx);
			idx += my_strlen(tab[count]) - 1;
			count++;
		}
		idx++;
	}
	tab[count] = NULL;
	return (tab);
}
