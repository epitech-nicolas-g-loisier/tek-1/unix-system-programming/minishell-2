/*
** EPITECH PROJECT, 2018
** Minishell
** File description:
** get command
*/

#include <stdlib.h>
#include "my.h"
#include "minishell.h"

int	count_command(char *buffer)
{
	int idx = 0;
	int count = 0;

	if (buffer == NULL)
		return (0);
	while (buffer[idx] != '\0') {
		if (buffer[idx] == ';' || buffer[idx] == '|')
			count++;
		else if (buffer[idx] == '<' || buffer[idx] == '>') {
			count++;
			if (buffer[idx] == buffer[(idx + 1)])
				idx++;
		}
		idx++;
	}
	count++;
	return (count);
}

int	get_next_command(char *buffer, int begin)
{
	int idx = begin;

	for (; buffer[idx] != '\0'; idx++) {
		if (buffer[idx] == ';') {
			buffer[idx] = '\0';
			return (++idx);
		} else if (buffer[idx] == '|') {
			buffer[idx] = '\0';
			return (-1);
		}
		if (buffer[idx] == '<' || buffer[idx] == '>') {
			buffer[idx] = '\0';
			if (buffer[idx] == buffer[(idx + 1)])
				idx++;
			return (-1);
		}
	}
	return (idx);
}

char	***move_exit(char ***command)
{
	char **tmp = NULL;
	int idx = 0;
	int tmp_idx = 0;

	if (command == NULL)
		return (command);
	while (command[idx] != NULL) {
		if (my_strcmp(command[idx][0], "exit") == 0) {
			tmp = command[idx];
			tmp_idx = idx;
		}
		idx++;
	}
	if (tmp != NULL) {
		command[tmp_idx] = command[(idx - 1)];
		command[(idx - 1)] = tmp;
	}
	return (command);
}

char	***get_all_command(char *buffer)
{
	int begin = 0;
	int end = 0;
	int idx = 0;
	int nb = count_command(buffer);
	char ***command = malloc(sizeof(char **) * (nb + 1));

	if (command == NULL)
		return (NULL);
	while (buffer[end] != '\0') {
		end = get_next_command(&buffer[0], begin);
		command[idx] = command_parser(&buffer[begin]);
		begin = end;
		idx++;
		if (end == -1)
			return (NULL);
	}
	command[idx] = NULL;
	command = move_exit(command);
	return (command);
}
