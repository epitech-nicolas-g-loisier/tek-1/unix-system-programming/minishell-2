/*
** EPITECH PROJECT, 2018
** Minishell1
** File description:
** env function
*/

#include <unistd.h>
#include <stdlib.h>
#include "my.h"
#include "minishell.h"

char	**alloc_env(char **old_env)
{
	char **new_env = NULL;
	int size = 0;

	if (old_env == NULL)
		return (NULL);
	while (old_env[size] != NULL) {
		size++;
	}
	new_env = malloc(sizeof(char*) * (size + 1));
	size = 0;
	while (old_env[size] != NULL) {
		new_env[size] = my_strdup(old_env[size]);
		if (new_env[size] == NULL)
			return (NULL);
		size++;
	}
	new_env[size] = NULL;
	return (new_env);
}

void	free_env(char **env)
{
	int idx = 0;

	while (env[idx] != NULL) {
		free(env[idx]);
		idx++;
	}
	free(env);
}

int	env_function(char **command, char **env)
{
	int idx = 0;

	if (command[1] != NULL) {
		write(2, "Error: env non require argument\n", 32);
		return (1);
	} else {
		while (env[idx]) {
			write(1, env[idx], my_strlen(env[idx]));
			write(1, "\n", 1);
			idx++;
		}
	}
	return (0);
}
