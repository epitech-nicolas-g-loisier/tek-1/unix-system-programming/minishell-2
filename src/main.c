/*
** EPITECH PROJECT, 2018
** Minishell 1
** File description:
** UNIX command interpreter
*/

#include <unistd.h>
#include "my.h"
#include "minishell.h"

int	main(int argc, char **argv, char **envp)
{
	int ret = 0;

	if (argc == 2 && my_strcmp(argv[1], "-h") == 0) {
		write(1, "mysh: UNIX command interpreter.\n", 32);
	}
	else if (argc > 1) {
		write(2, "mysh: invalid usage: retry with -h\n", 35);
		return (84);
	}
	else {
		ret = launch_minishell(envp);
	}
	return (ret);
}
