/*
** EPITECH PROJECT, 2018
** Minishell1
** File description:
** exit function
*/

#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "minishell.h"

int	exit_function(char **command, char ***env, int old_ret)
{
	int ret = 0;

	if (command[1] == NULL) {
		write(1, "exit\n", 5);
		free_env(*env);
		exit(old_ret);
	} else {
		if (my_str_isnum(command[1]) == 0){
			write(2, "exit: Badly formed number.\n", 27);
		} else {
			write(1, "exit\n", 5);
			ret = my_getnbr(command[1]);
			free_env(*env);
			exit(ret);
		}
	}
	return (1);
}
