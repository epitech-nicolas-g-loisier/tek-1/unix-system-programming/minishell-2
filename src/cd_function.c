/*
** EPITECH PROJECT, 2018
** Minishell1
** File description:
** cd function
*/

#include <unistd.h>
#include <stdlib.h>
#include <linux/limits.h>
#include "my.h"
#include "minishell.h"

char	*get_oldpwd(char *oldpwd)
{
	static char *pwd = NULL;
	char *tmp = my_strdup(pwd);

	if (oldpwd != NULL) {
		free(pwd);
		pwd = my_strdup(oldpwd);
	}
	return (tmp);
}

int	modif_oldpwd(char **env, char *old_dir)
{
	int idx = 0;

	while (env[idx] != NULL) {
		if (find_name(env[idx], "OLDPWD") == 0) {
			break;
		}
		idx++;
	}
	if (env[idx] == NULL || old_dir == NULL)
		return (84);
	free(env[idx]);
	env[idx] = my_strcat("OLDPWD=", old_dir);
	get_oldpwd(old_dir);
	free(old_dir);
	return (0);
}

char	*go_to_home(char **env)
{
	int idx = 0;
	char *dir = malloc(sizeof(char) * PATH_MAX + 1);

	dir = getcwd(dir, PATH_MAX);
	while (env[idx] != NULL) {
		if (find_name(env[idx], "HOME") == 0) {
			break;
		}
		idx++;
	}
	if (env[idx] != NULL){
		chdir(my_strdup(&env[idx][5]));
		modif_oldpwd(&env[0], dir);
	} else
		free(dir);
	return (0);
}

int	go_to_oldpwd(char **env)
{
	char *tmp = malloc(sizeof(char) * PATH_MAX + 1);
	int idx = 0;

	while (env[idx] != NULL) {
		if (find_name(env[idx], "OLDPWD") == 0) {
			break;
		}
		idx++;
	}
	tmp = getcwd(tmp, PATH_MAX);
	if (env[idx] == NULL && get_oldpwd(NULL) == NULL) {
		write(2, ": No such file or directory.\n", 29);
		return (1);
	} else if (env[idx] == NULL) {
		chdir(get_oldpwd(tmp));
		free(tmp);
	} else {
		chdir(&env[idx][7]);
		modif_oldpwd(&env[0], tmp);
	}
	return (0);
}

int	cd_function(char **command, char **env)
{
	char *old_dir = NULL;
	int check = 0;
	int ret = 0;

	if (command[1] == NULL || my_strcmp("~", command[1]) == 0){
		go_to_home(&env[0]);
	} else if (my_strcmp("-", command[1]) == 0) {
		ret = go_to_oldpwd(&env[0]);
	} else {
		old_dir = malloc(sizeof(char) * PATH_MAX + 1);
		modif_oldpwd(&env[0], getcwd(old_dir, PATH_MAX));
		check = chdir(command[1]);
	}
	if (check == -1) {
		write(2, command[0], my_strlen(command[0]));
		write(2, ": Not a directory.\n", 19);
		ret = 1;
	}
	return (ret);
}
