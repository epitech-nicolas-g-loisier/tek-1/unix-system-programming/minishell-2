/*
** EPITECH PROJECT, 2018
** Minishell1
** File description:
** setenv function
*/

#include <unistd.h>
#include <stdlib.h>
#include "my.h"
#include "macro.h"
#include "minishell.h"

char	*get_name(char *name, char *value)
{
	int idx = 0;
	int length = my_strlen(name) + my_strlen(value);
	char *new_name = malloc(sizeof(char) * (length + 2));

	if (new_name == NULL)
		return (NULL);
	while (name[idx] != '\0') {
		if (name[idx] == '=')
			return (NULL);
		new_name[idx] = name[idx];
		idx++;
	}
	new_name[idx++] = '=';
	if (value != NULL) {
		for (int idx2 = 0; value[idx2] != '\0'; idx2++) {
			new_name[idx] = value[idx2];
			idx++;
		}
	}
	new_name[idx] = '\0';
	return (new_name);
}

int	check_name(char *line, char *name)
{
	int idx = 0;

	while (name[idx] != '\0') {
		if (line[idx] != name[idx])
			return (1);
		idx++;
	}
	if (name[idx] == '\0' && line[idx] == '=') {
		return (0);
	} else {
		return (1);
	}
}

int	check_setenv_char(char *name)
{
	int egal = 0;

	for (int idx = 0; name[idx] != '\0'; idx++) {
		if (name[idx] == '=' && egal == 0) {
			egal = 1;
			idx++;
		}
		if (!IS_ALPHA(name[idx]) && !IS_NUM(name[idx]) &&
			name[idx] != '_') {
			write(2, "setenv: Variable name must contain", 34);
			write(2, " alphanumeric characters.\n", 26);
			return (0);
		}
	}
	return (1);
}

char	**get_new_env(int size, char **env, char *name)
{
	int line_idx = 0;
	char **new_env = malloc(sizeof(char*) * (size + 2));

	if (new_env == NULL || env == NULL)
		return (NULL);
	while (env[line_idx] != NULL) {
		new_env[line_idx] = env[line_idx];
		line_idx++;
	}
	new_env[line_idx] = name;
	new_env[(line_idx + 1)] = NULL;
	return (new_env);
}

char	**setenv_function(char **command, char **env, int *ret)
{
	char **new_env;
	char *name;
	int idx = 0;

	if (command[1] == NULL) {
		env_function(command, env);
		return (env);
	}
	name = get_name(command[1], command[2]);
	if (name == NULL || check_setenv_char(name) == 0) {
		*ret = 1;
		return (env);
	}
	while (env[idx] != NULL) {
		if (check_name(env[idx], command[1]) == 0) {
			env[idx] = name;
			return (env);
		}
		idx++;
	}
	new_env = get_new_env(idx, env, name);
	free(env);
	return (new_env);
}
