/*
** EPITECH PROJECT, 2018
** Minishell
** File description:
** get the return value of the command
*/

#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "my.h"
#include "minishell.h"
#include <errno.h>

static int	errno_error(char const * const command)
{
	if (errno == ENOEXEC) {
		write(2, command, my_strlen(command));
		write(2, ": Exec format error. Wrong Architecture.\n", 41);
	}
	if (errno == EACCES) {
		write(2, command, my_strlen(command));
		write(2, ": Permission denied.\n", 21);
	}
	kill(getpid(), SIGTERM);
	return (1);
}

static int	check_signal(int status)
{
	if (WIFSIGNALED(status)) {
		if (WTERMSIG(status) == SIGSEGV) {
			write(2, "Segmentation fault\n", 19);
			return (139);
		} else if (WTERMSIG(status) == SIGFPE) {
			write(2, "Floating exception\n", 19);
			return (136);
		}
		if (WTERMSIG(status) == 15) {
			return (1);
		}
	}
	return (WEXITSTATUS(status));
}

int	get_return_value(int error,int status, char *command)
{
	int ret = 0;

	if (error == -1)
		ret = errno_error(command);
	else
		ret = check_signal(status);
	return (ret);
}
