/*
** EPITECH PROJECT, 2018
** Minishell1
** File description:
** function launch with the binary
*/

#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "my.h"
#include "minishell.h"

int	check_access(char *command)
{
	if (access(command, X_OK) != 0) {
		write(2, command, my_strlen(command));
		write(2, ": Command not found.\n", 21);
		return (0);
	}
	return (1);
}

int	launch_binary(char **command, char **env)
{
	pid_t child_pid;
	pid_t wpid;
	int status = 50;
	int error = 0;

	if (check_access(command[0]) == 0) {
		return (1);
	}
	child_pid = fork();
	if (child_pid == -1) {
		return (1);
	} else if (child_pid == 0) {
		error = execve(command[0], command, env);
	} else {
		wpid = waitpid(child_pid, &status, WUNTRACED);
		if (wpid == -1) {
			return (1);
		}
	}
	return (get_return_value(error, status, command[0]));
}

int	find_prog(char **command, char **env)
{
	char *path = get_path_line(env);
	char **path_tab = path_parser(path);
	char *prog = NULL;
	int ret = 0;

	if (path == NULL)
		return (1);
	for (int idx = 0; path_tab[idx] != NULL; idx++) {
		prog = my_strcat(path_tab[idx], command[0]);
		if (access(prog, X_OK) == 0) {
			free(command[0]);
			command[0] = prog;
			ret = launch_binary(command, env);
			return (ret);
		}
		free(prog);
	}
	write(2, command[0], my_strlen(command[0]));
	write(2, ": Command not found.\n", 22);
	return (1);
}

int	other_function(char **command, char **env)
{
	int idx = 0;
	int ret = 0;

	while (command[0][idx] != '\0') {
		if (command[0][idx] == '/') {
			ret = launch_binary(command, env);
			return (ret);
		}
		idx++;
	}
	ret = find_prog(command, env);
	return (ret);
}
