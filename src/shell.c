/*
** EPITECH PROJECT, 2018
** Minishell
** File description:
** main function of minishell
*/

#include <unistd.h>
#include <stdlib.h>
#include <linux/limits.h>
#include "my.h"
#include "get_next_line.h"
#include "minishell.h"

int	spe_env_function(char **command, char ***env, int idx)
{
	int ret = 0;

	if (idx == 3) {
		*env = setenv_function(command, *env, &ret);
	} else if (idx == 4){
		*env = unsetenv_function(command, *env, &ret);
	}
	if (*env == NULL)
		return (1);
	return (ret);
}

int	choose_command(char **command, char ***env)
{
	static int ret = 0;
	char *function[6] = {"cd", "env", "exit", "setenv", "unsetenv", NULL};
	int (*funct_tab[6])(char **, char **);
	int idx = 0;

	funct_tab[0] = *cd_function;
	funct_tab[1] = *env_function;
	funct_tab[5] = *other_function;
	while (function[idx]) {
		if (my_strcmp(function[idx], command[0]) == 0)
			break;
		idx++;
	}
	if (idx < 2 || idx == 5) {
		ret = funct_tab[idx](command, *env);
	} else if (idx == 2) {
		ret = exit_function(command, env, ret);
	} else
		ret = spe_env_function(command, env, idx);
	return (ret);
}

int	write_prompt(void)
{
	char *name = malloc(sizeof(char) * PATH_MAX + 1);

	if (name != NULL) {
		name = getcwd(name, PATH_MAX);
		write(1, name, my_strlen(name));
		free(name);
	}
	write(1, " > ", 3);
	return (0);
}

int	minishell(char *buffer, char ***env)
{
	char ***command;
	int ret = 0;

	command = get_all_command(buffer);
	if (command == NULL)
		return (84);
	for (int idx = 0; command[idx] != NULL; idx++) {
		ret = choose_command(command[idx], env);
	}
	free_command(command);
	free(buffer);
	return (ret);
}

int	launch_minishell(char **env)
{
	char *buffer = NULL;
	int ret = 0;

	env = alloc_env(env);
	while (env && env[0]){
		write_prompt();
		buffer = get_next_line(0);
		if (buffer == NULL)
			break;
		else if (my_strcmp(buffer, "") != 0) {
			ret = minishell(buffer, &env);
		} else
			free(buffer);
	}
	if (env != NULL)
		free_env(env);
	return (ret);
}
