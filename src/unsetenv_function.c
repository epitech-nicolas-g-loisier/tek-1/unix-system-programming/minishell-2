/*
** EPITECH PROJECT, 2018
** Minishell1
** File description:
** unsetenv function
*/

#include <unistd.h>
#include <stdlib.h>
#include "my.h"
#include "minishell.h"

int	find_name(char *line, char *name)
{
	int idx = 0;

	while (name[idx] != '\0') {
		if (line[idx] != name[idx])
			return (1);
		idx++;
	}
	if (name[idx] == '\0' && line[idx] == '=') {
		return (0);
	} else {
		return (1);
	}
}

char	**delete_name(int stop, char **env)
{
	int idx = 0;
	int idx2 = 0;
	char **new_env;

	while (env[idx]) {
		idx++;
	}
	new_env = malloc(sizeof(char*) * (idx + 1));
	idx = 0;
	while (env[idx2] != NULL) {
		if (idx2 == stop) {
			idx2++;
		} else {
			new_env[idx] = env[idx2];
			idx++;
			idx2++;
		}
	}
	new_env[idx] = NULL;
	return (new_env);
}

char	**unsetenv_function(char **command, char **env, int* ret)
{
	char **new_env;
	char *name;
	int idx = 0;

	if (command[1] == NULL) {
		write(2, "unsetenv: Too few arguments.\n", 29);
		*ret = 1;
		return (env);
	}
	name = my_strdup(command[1]);
	if (name == NULL)
		return (env);
	while (env[idx]) {
		if (find_name(env[idx], command[1]) == 0) {
			new_env = delete_name(idx, env);
			free(env);
			return (new_env);
		}
		idx++;
	}
	return (env);
}
