/*
** EPITECH PROJECT, 2018
** Minishell
** File description:
** free functions
*/

#include <stdlib.h>

int	free_command(char ***command)
{
	if (command == NULL)
		return (0);
	for (int idx = 0; command[idx] != NULL; idx++) {
		for (int idx2 = 0; command[idx][idx2] != NULL; idx2++)
			free(command[idx][idx2]);
		free(command[idx]);
	}
	free(command);
	return (0);
}
